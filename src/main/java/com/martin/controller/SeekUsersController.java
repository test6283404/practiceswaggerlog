package com.martin.controller;


import com.martin.model.User;
import com.martin.service.SeekUsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The user API
 *
 * @Author Martin
 * @since JDK 11
 */
@RestController
@Api(tags = "Seek Users API")
public class SeekUsersController {

    @Autowired
    private SeekUsersService seekUsersService;

    Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Accept the param in order to get the specific info of user
     * @param userId
     * @return User object
     */
    @Operation(summary = "Find the specific user", description = "Find the specific user")
    @ApiResponses({
            @ApiResponse(code=200,message="成功查詢特定使用者"),
            @ApiResponse(code=400,message="oops, 你輸入錯誤")})
    @GetMapping("/find/user")
    public ResponseEntity<User> findUser(@RequestParam("userId") Integer userId) {
        log.info("您正在使用 findUser API");

        User specificUser = seekUsersService.findSpecificUser(userId);

        if (specificUser != null) {
            log.info("User 存在!");

            return ResponseEntity.ok(specificUser);
        } else {
            log.error("User 並不存在!");

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}
