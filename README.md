程式的使用說明書
===

程式概述
---
這是用來查詢特定User的程式，您將輸入特定的**UserID(1~12)**，得到的結果會是該User的**名與姓**，以及該User的**email**
。同時，輸入其他內容
(非1~12)，將會**終止程式**，這點還請您多多留意。

程式結構說明
---
以 MVC 架構完成，分別為 Controller Service Model(Users, User) Config.

Config 部分是:

    1. 我寫了 SpringFox 的配置，使用 SWAGGER_2 

Controller 部分是:

    1. 當有人呼叫此 api，即可觸發 log 日誌，來記錄呼叫次數以及成功或失敗的紀錄。同時，我使用 swagger_2 來客製化我想要表達的內容，包含 api
        的名稱，以及客製化的 HTTP狀態碼。
    2. 調用 SeekUsersService 來完成需要判斷的邏輯。
    3. 將會接收 Service 所傳的 User object，並回傳 ResponseEntity<User> 給呼叫此 api 的使用者 

Service 部分是:

    1. api 主要的邏輯。
    2. 以與預設好的url用拼接 String 的方式，得到特定的網址
    4. URI 使用 create(特定的網址)，藉由 HttpRequest newBuilder(URI).build() 來得到特定的 HttpRequest
    5. 透過 HttpClient newHttpClient().send(請求, 回復).body() 取得 String 字串
    6. 用 gson JsonParser parse(字串).getAsJsonObject() 將回傳的 String 轉成 JsonObject
    7. 用 gson JsonObject jsonObject.getAsJsonObject("你要的 json key") 取得特定 json
    8. 最後，用 Gson().fromJson(特定 json, 特定物件.class) 取得 特定物件
    9. 使用特定物件 getter and setter 封裝到 User 並回傳

Users 部分是:

    1. 屬於特定物件，有關於您所接收的 json value
    2. 使用 gson @SerializedName 來解決 java 命名規則

User 部分是:

    1. 用來封裝需要回傳的資訊

Maven 配置
---

* 添加 dependency

      <dependency>
      <groupId>com.google.code.gson</groupId>
       <artifactId>gson</artifactId>
       <version>2.10.1</version>
       </dependency>

      <dependency>
      <groupId>io.springfox</groupId>
       <artifactId>springfox-swagger2</artifactId>
      <version>3.0.0</version>
      </dependency>

        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-boot-starter</artifactId>
            <version>3.0.0</version>
        </dependency>

        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>3.0.0</version>
        </dependency>

操作說明
---

### 從 git 下載

1. git bush 執行在您想要的位置
   `$ git clone https://gitlab.com/test6283404/test001.git`
2. PowerShell 執行
    - `cd 到您 clone 的位置`
    - `mvn clean compile assembly:single`
    - `java -jar target/my-app-1.0-SNAPSHOT-jar-with-dependencies.jar`

注意事項
---
JDK 版本配置

`java version "17.0.7" 2023-04-18 LTS`

MAVEN 版本配置

`Apache Maven 3.9.6`